<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public static $wrap = null;
    public $status;
    public $message;

    public function __construct($resource, $status, $message)
    {
        parent::__construct($resource);
        $this->status = $status;
        $this->message = $message;
    }
    public function toArray($request)
    {
        return [
            'status'        => $this->status,
            'users'         => $this->resource,
            'message'       => $this->message,
        ];
    }
}
