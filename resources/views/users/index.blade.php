@extends('layouts.app')

@section('title', 'Users')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ __('Users') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Users') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>


    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('users.create') }}" class="btn btn-sm btn-primary"
                                title="Create">{{ __('Create Users') }}<i class="fas fa-plus"></i></a>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered" id="dataTable">
                                <thead>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Code</th>
                                    <th>QR</th>
                                    <th>Masking</th>
                                    <th>Pasing</th>
                                    <th>Action</th>
                                </thead>

                                <tbody>
                                    @foreach ($users as $item)
                                         <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item['name'] }}</td>
                                            <td>{{ $item['email'] }}</td>
                                            <td>{{ $item['code'] }}</td>
                                            <td>
                                                {{ \SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)->generate($item['code']) }}
                                            </td>
                                            <td> {{ substr_replace($item['code'], '*****', 3, -3) }}</td>
                                            <td> {{ preg_replace('/[^a-zA-Z]/', '', $item['code']) }}
                                            </td>
                                            <td>
                                                <a href="{{ route('users.edit', $item['id']) }}"
                                                    class="btn btn-xs btn-warning">EDIT <i class='fas fa-edit'></i></a>
                                                <form action="{{ route('users.destroy', $item['id']) }}"
                                                    class='ml-1 delete-form' method='POST'>
                                                    @csrf
                                                    <input type='hidden' name='_method' value='delete'>
                                                    <button class='btn btn-danger btn-xs'> DELETE <i
                                                            class='fas fa-trash'></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
