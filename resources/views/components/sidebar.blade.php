<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{ asset('img/black_owl.png') }}" alt=" Logo" class="brand-image">
        <span class="brand-text font-weight-light">{{ config('app.name') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('img/default.png') }}" class="img-circle " alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Str::upper(Auth::user()->name) ?? 'N/A' }}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">

                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link @if (request()->is('home*')) active @endif"><i
                            class="nav-icon fas fa-home"></i>
                        <p>{{ __('Home') }}</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('users.index') }}"
                        class="nav-link @if (request()->is('users*')) active @endif"><i
                            class="nav-icon fas fa-tasks"></i>
                        <p>{{ __('Users') }}</p>
                    </a>
                </li>


                <li class="nav-item">
                    <a data-toggle="modal" data-target="#logoutModal" class="nav-link"><i
                            class="nav-icon fas fa-sign-in-alt"></i>
                        <p>{{ __('Logout') }}</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
