<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class RequestService
{
    protected
        $url,
        $headers = [],
        $methods = 'GET';

    public function __construct()
    {
        $this->url = config('app.api_url');
        $this->headers = [
            'Accept' => 'application/json',
        ];
    }

    public function get(string $uri, Request $request)
    {
        $requestData = $this->getAllRequest($request);
        $this->methods = 'GET';

        return $this->dispatchRequest($uri, $requestData);
    }

    public function post(string $uri, Request $request)
    {
        $requestData = $this->getAllRequest($request);
        $this->methods = 'POST';

        return $this->dispatchRequest($uri, $requestData);
    }

    public function put(string $uri, Request $request)
    {
        $requestData = $this->getAllRequest($request);
        $this->methods = 'PUT';

        return $this->dispatchRequest($uri, $requestData);
    }

    public function delete(string $uri, Request $request)
    {
        $requestData = $this->getAllRequest($request);
        $this->methods = 'DELETE';

        return $this->dispatchRequest($uri, $requestData);
    }

    private function dispatchRequest(String $uri, array $data = [])
    {

        $methods = strtolower($this->methods);
        $response = Http::withHeaders($this->headers);

        try {
            $response = $response->$methods($this->url . $uri, $data['input'] ?? []);

            if ($response->successful())
                return array_merge(['success' => true], $response->json());
        } catch (\Throwable $e) {
            return [
                'success' => false,
                'message' => 'Server  Error',
                'erro' => $e->getMessage()
            ];
        }
    }

    private function getAllRequest(Request $request): array
    {
        $data = [];
        foreach ($request->all() as $key => $value) {
            if ($request->hasFile($key)) {
                $data['attachment'][$key] = $request->file($key);
            } else {
                $data['input'][$key] = $value;
            }
        }

        return $data;
    }

    public function setUrl(string $url): RequestService
    {
        $this->url = $url;
        return $this;
    }

    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
        return $this;
    }
}
