<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();
        return new UserResource($users, Response::HTTP_OK, 'Users GET successfully.');
    }


    public function store(UserStoreRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'code' => Str::random(10),
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        return new UserResource($user, Response::HTTP_CREATED, 'User created successfully.');
    }


    public function show(string $id)
    {
        $user = User::findOrFail($id);
        return new UserResource($user, Response::HTTP_OK, 'User GET successfully.');
    }


    public function update(UserUpdateRequest $request, string $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        return new UserResource($user, Response::HTTP_OK, 'User updated successfully.');
    }


    public function destroy(string $id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return new UserResource($user, Response::HTTP_OK, 'User deleted successfully.');
    }
}
