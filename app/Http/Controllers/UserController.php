<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\RequestService;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;

class UserController extends Controller
{
    protected $uri;
    protected $service;
    public function __construct(RequestService $service)
    {
        $this->uri = config('app.api_url');
        $this->service = $service;
    }

    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }


    public function create()
    {
        return view('users.create');
    }


    public function store(UserStoreRequest $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'code' => Str::random(10),
            'password' => bcrypt($request->password),
        ]);

        Alert::success('Success', 'User created successfully');
        return redirect()->route('users.index');
    }


    public function edit(string $id)
    {
        $user = User::findOrFail($id);

        return view('users.edit', compact('user'));
    }


    public function update(UserUpdateRequest $request, string $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        Alert::success('Success', 'User updated successfully');
        return redirect()->route('users.index');
    }


    public function destroy(string $id)
    {
        $user = User::findOrFail($id);

        if (Auth::user()->id == $user->id) {
            Alert::error('Error', 'This user is currently logged in, cannot be deleted');
            return redirect()->route('users.index');
        }
        $user->delete();

        Alert::success('Success', 'User deleted successfully');
        return redirect()->route('users.index');
    }
}
