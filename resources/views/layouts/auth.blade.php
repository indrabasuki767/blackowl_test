<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} | @yield('title')</title>

    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">

    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <style>
        body,
        html {
            height: 100%;
        }

        .auth-background {
            /* The image used */
            background-image: url("{{ asset('img/puzzle.png') }}");

            /* Full height */
            height: 100%;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            opacity: 0.9;
        }
    </style>
    @stack('styles')
</head>

<body style="overflow-y: hidden; overflow-x: hidden" class="auth-background">
    @yield('content')

    @include('sweetalert::alert')

    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <script>
        $(".alert").fadeTo(3000, 500).slideUp(500, function() {
            $(".alert").alert('close');
        });
    </script>
    @stack('scripts')
</body>

</html>
