@extends('layouts.auth')

@section('title', 'Login')

@section('content')
    <section class="h-100 ">
        <div class="container h-100">
            <div class="row justify-content-sm-center h-100">
                <div class="col-xxl-4 col-xl-5 col-lg-5 col-md-7 col-sm-9 mt-5">
                    <div class="text-center my-5">
                        <img src="{{ asset('img/black_owl.png') }}" alt="logo" width="100">
                    </div>
                    <div class="card shadow-lg mt-5">
                        <div class="card-body p-5">
                            <p class="m-0"
                                style="color: #1143D8; font-size: 32px;text-align:center; letter-spacing: 4px;">
                                <b>LOGIN</b>
                            </p>

                            <form action="{{ route('login') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="email" style="font-size: 16px;">{{ __('Email') }}</label>
                                    <input id="email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus
                                        placeholder="Email">

                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group mt-3 mb-4">
                                    <label for="">{{ __('Password') }}</label>
                                    <div class="input-group mb-3" id="show_hide">
                                        <input type="password" class="form-control" name="password" placeholder="Password">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span style="cursor:pointer" class="fa fa-eye-slash"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <button class="form-control mt-2"
                                    style="background-color:#3761DE;color: white;border-radius:4px;"
                                    type="submit"><b>{{ __('LOGIN') }}</b>
                                </button>
                            </form>


                            <p class="mb-0 text-center my-2">
                                Don’t have any account? <a href="{{ route('register') }}" title="Login"
                                    class="text-center">
                                    Register
                                </a>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $("#show_hide span").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide input').attr("type") == "text") {
                    $('#show_hide input').attr('type', 'password');
                    $('#show_hide span').addClass("fa fa-eye-slash");
                    $('#show_hide span').removeClass("fas fa-eye");
                } else if ($('#show_hide input').attr("type") == "password") {
                    $('#show_hide input').attr('type', 'text');
                    $('#show_hide span').removeClass("fa fa-eye-slash");
                    $('#show_hide span').addClass("fas fa-eye");
                }
            });
        });
    </script>
@endpush
