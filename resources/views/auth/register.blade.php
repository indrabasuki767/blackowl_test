@extends('layouts.auth')

@section('title', 'Register')

@section('content')
    <section class="h-100 ">
        <div class="container h-100">
            <div class="row justify-content-sm-center h-100">
                <div class="col-xxl-4 col-xl-5 col-lg-5 col-md-7 col-sm-9 mt-5">
                    <div class="text-center my-5">
                        <img src="{{ asset('img/black_owl.png') }}" alt="logo" width="100">
                    </div>
                    <div class="card">
                        <div class="card-body login-card-body">
                            <p class="m-0 mb-3"
                                style="color: #1143D8; font-size: 32px;text-align:center; letter-spacing: 4px;">
                                <b>FORM REGISTER</b>
                            </p>

                            <form action="{{ route('register') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                                        value="{{ old('name') }}" name="name" placeholder="Full name">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <input type="text" class="form-control @error('email') is-invalid @enderror"
                                        value="{{ old('email') }}" name="email" placeholder="Email address">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control  @error('phone') is-invalid @enderror"
                                        name="phone" placeholder="Phone number" value="{{ old('phone') }}">
                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="input-group" id="show_hide">
                                        <input type="password" class="form-control  @error('password') is-invalid @enderror"
                                            name="password" placeholder="Password">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span style="cursor:pointer" class="fa fa-eye-slash"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group" id="show_hide_confirm">
                                        <input type="password" class="form-control" name="password_confirmation"
                                            placeholder="Confirmation password">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span style="cursor:pointer" class="fa fa-eye-slash"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">REGISTER</button>
                            </form>


                            <p class="mb-0 text-center my-2">
                                Have an account? <a href="{{ route('login') }}" title="Login" class="text-center">
                                    Login
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $("#show_hide span").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide input').attr("type") == "text") {
                    $('#show_hide input').attr('type', 'password');
                    $('#show_hide span').addClass("fa fa-eye-slash");
                    $('#show_hide span').removeClass("fas fa-eye");
                } else if ($('#show_hide input').attr("type") == "password") {
                    $('#show_hide input').attr('type', 'text');
                    $('#show_hide span').removeClass("fa fa-eye-slash");
                    $('#show_hide span').addClass("fas fa-eye");
                }
            });

            $("#show_hide_confirm span").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_confirm input').attr("type") == "text") {
                    $('#show_hide_confirm input').attr('type', 'password');
                    $('#show_hide_confirm span').addClass("fa fa-eye-slash");
                    $('#show_hide_confirm span').removeClass("fas fa-eye");
                } else if ($('#show_hide_confirm input').attr("type") == "password") {
                    $('#show_hide_confirm input').attr('type', 'text');
                    $('#show_hide_confirm span').removeClass("fa fa-eye-slash");
                    $('#show_hide_confirm span').addClass("fas fa-eye");
                }
            });
        });
    </script>
@endpush
